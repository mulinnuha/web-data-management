# ======================== INSTALL MONGODB (Windows) ======================
# 1) Install MongoDB: Go to 'https://www.mongodb.com/download-center#community'
#                     Select "Windows Server 2008 R2 64-bit and later, with SSL support x64"
# 2) Open mongod.exe (C:/Program Files/MongoDB/Server/3.4/bin/mongod.exe)
# 3) Open mongo.exe  (C:/Program Files/MongoDB/Server/3.4/bin/mongo.exe)
# 4) Type command 'use wdm' in mongo.exe to create empty database with name 'wdm'
# =========================================================================

# ======================= POSTGRES TO MONGODB =======================
# 1) Use this script to produce 'movies.json' and 'actors.json' (takes about 30 and 30 minutes)
# 2) Open command prompt
#    Type command 'cd C:/Program Files/MongoDB/Server/3.4/bin' to change directory
#    Type the following commands to import the data from json:
#      mongoimport --db wdm --collection movies --file C:/.../movies.json --jsonArray
#      mongoimport --db wdm --collection actors --file C:/.../actors.json --jsonArray
# 3) Use this script to create indexes on your mongo database
# ===================================================================

import json
import psycopg2 as pg
from pymongo import MongoClient
from util import lists_with_labels, merge_list


def create_movies_json(output_json_name, connection_string):
    """
    Use this method to produce a json file (jsonArray format)
    that contains all movie-related data (for MongoDB use).
    Takes about 30 minutes.

    :param output_json_name: File name of the output json file
    :param connection_string: Connection string to connect with the IMDB Postgres database
    """
    with open(output_json_name, 'w') as output_json_file:
        conn = pg.connect(connection_string)  # connect to the postgres database
        cur = conn.cursor()  # cursor used to execute queries

        first_id = 1
        last_id = 1356171

        for id in range(first_id, last_id + 1):
            print "Movie with ID: " + str(id) + " is being processed."

            # Movie info
            q_movie_info = """ SELECT m.idmovies, m.title, m.year, m.number, m.type, m.location, m.language
                               FROM movies AS m
                               WHERE m.idmovies = %(id)s """

            # series
            q_movie_series = """ SELECT s.idseries, s.name, s.season, s.number
                                 FROM series AS s
                                 WHERE s.idmovies = %(id)s """

            # genres
            q_movie_genres = """ SELECT DISTINCT g.genre
                                 FROM genres AS g
                                 JOIN movies_genres AS mg
                                 ON g.idgenres = mg.idgenres
                                 WHERE mg.idmovies = %(id)s """  # Might need DISTINCT

            # keywords
            q_movie_keywords = """ SELECT DISTINCT k.keyword
                                   FROM keywords AS k
                                   JOIN movies_keywords AS mk
                                   ON k.idkeywords = mk.idkeywords
                                   WHERE mk.idmovies = %(id)s """  # DISTINCT because it has multiple entries...

            # actor ids
            q_movie_actor_ids = """ SELECT DISTINCT ai.idactors
                                    FROM acted_in AS ai
                                    WHERE ai.idmovies = %(id)s """

            result_dict = {}

            # movie info
            cur.execute(q_movie_info, {'id': id})
            row = cur.fetchall()[0]  # only 1 row output, since ids are unique
            result_dict['idmovies'] = row[0] if row[0] is not None else ''
            result_dict['title'] = row[1] if row[1] is not None else ''
            result_dict['year'] = row[2] if row[2] is not None else ''
            result_dict['number'] = row[3] if row[3] is not None else ''
            result_dict['type'] = row[4] if row[4] is not None else ''
            result_dict['location'] = row[5] if row[5] is not None else ''
            result_dict['language'] = row[6] if row[6] is not None else ''

            # series
            cur.execute(q_movie_series, {'id': id})
            rows = cur.fetchall()
            result_dict['series'] = lists_with_labels(rows, ['idseries', 'name', 'season', 'number'])

            # genres
            cur.execute(q_movie_genres, {'id': id})
            rows = cur.fetchall()
            result_dict['genres'] = merge_list(rows)

            # keywords
            cur.execute(q_movie_keywords, {'id': id})
            rows = cur.fetchall()
            result_dict['keywords'] = merge_list(rows)

            # actor_ids
            cur.execute(q_movie_actor_ids, {'id': id})
            rows = cur.fetchall()
            result_dict['actor_ids'] = merge_list(rows)

            # Write to disk
            if id == first_id:
                output_json_file.write('[')
            output_json_file.write(json.dumps(result_dict))
            if id == last_id:
                output_json_file.write(']')
            else:
                output_json_file.write(',')


def create_actors_json(output_json_name, connection_string):
    """
    Use this method to produce a json file (jsonArray format)
    that contains all actor-related data (for MongoDB use).
    Takes about 30 minutes.

    :param output_json_name: File name of the output json file
    :param connection_string: Connection string to connect with the IMDB Postgres database
    """
    with open(output_json_name, 'w') as output_json_file:
        conn = pg.connect(connection_string)  # connect to the postgres database
        cur = conn.cursor()  # cursor used to execute queries

        first_id = 1
        last_id = 3656641

        for id in range(first_id, last_id + 1):
            print "Actor with ID: " + str(id) + " is being processed."

            # Actor info
            q_actor_info = """ SELECT a.idactors, a.fname, a.mname, a.lname, a.gender, a.number
                               FROM actors AS a
                               WHERE a.idactors = %(id)s """

            # acted_in
            q_acted_in = """ SELECT DISTINCT ai.idmovies, ai.idseries, ai.character, ai.billing_position
                             FROM acted_in AS ai
                             WHERE ai.idactors = %(id)s """

            result_dict = {}

            # Actor info
            cur.execute(q_actor_info, {'id': id})
            row = cur.fetchall()[0]  # only 1 row output, since ids are unique
            result_dict['idactors'] = row[0] if row[0] is not None else ''
            result_dict['fname'] = row[1] if row[1] is not None else ''
            result_dict['mname'] = row[2] if row[2] is not None else ''
            result_dict['lname'] = row[3] if row[3] is not None else ''
            result_dict['gender'] = row[4] if row[4] is not None else ''
            result_dict['number'] = row[5] if row[5] is not None else ''

            # acted_in
            cur.execute(q_acted_in, {'id': id})
            rows = cur.fetchall()
            result_dict['acted_in'] = lists_with_labels(rows, ['idmovies', 'idseries', 'character', 'billing_position'])

            # Write to disk
            if id == first_id:
                output_json_file.write('[')
            output_json_file.write(json.dumps(result_dict))
            if id == last_id:
                output_json_file.write(']')
            else:
                output_json_file.write(',')


def create_indexes(host, port, db_name):
    """
    Creates indexes on your mongo database

    :param host: Mongodb host
    :param port: Mongodb port
    :param db: Mongo database name
    """
    mongo_client = MongoClient(host, port)  # connect to the mongo database
    db = mongo_client[db_name]

    print 'Creating indexes...'

    db.movies.create_index('idmovies', unique=True)
    db.actors.create_index('idactors', unique=True)

    # We don't need text indexes as they are automatically created...

    print db.movies.index_information()
    print db.actors.index_information()


# create_movies_json('movies.json', "user=postgres password=s host=localhost port=5433 dbname=wdm")  # Produce movies json
# create_actors_json('actors.json', "user=postgres password=s host=localhost port=5433 dbname=wdm")  # Produce actors json
# create_indexes('localhost', 27017, 'wdm')  # Create indexes on your mongo database
