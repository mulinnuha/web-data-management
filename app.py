from neo4j.v1 import GraphDatabase, basic_auth
from flask_restful import Api
from flask import Flask, Blueprint
from pymongo import MongoClient
import psycopg2 as pg

from PostgresQueryHandler import PostgresQueryHandlerSC1, PostgresQueryHandlerSC2, PostgresQueryHandlerSC3, PostgresQueryHandlerSC4, PostgresQueryHandlerSC5
from MongoQueryHandler import MongoQueryHandlerSC1, MongoQueryHandlerSC2, MongoQueryHandlerSC3, MongoQueryHandlerSC4, MongoQueryHandlerSC5
from Neo4jQueryHandler import Neo4jQueryHandler

# Connect to the Postgres database
conn_postgres = pg.connect("user=postgres password=postgres host=52.59.40.1 port=5432 dbname=imdb_data")
cur_postgres = conn_postgres.cursor()  # Cursor used to execute queries

# Connect to the Mongo database
mongo_client = MongoClient('52.59.40.1', 27017)
db_mongo = mongo_client['wdm']

# Connect to Neo4j database
driver_neo4j = GraphDatabase.driver("bolt://52.59.40.1:7687", auth=basic_auth("neo4j", "neo4j123"))
session_neo4j = driver_neo4j.session()

# Default stuff...
api_bp = Blueprint('api', __name__)
api = Api(api_bp)
app = Flask(__name__)
app.register_blueprint(api_bp)

# Add the endpoints
# Postgres
api.add_resource(PostgresQueryHandlerSC1, '/postgres/sc1/', resource_class_kwargs={'cur': cur_postgres})  # add class property 'cur'
api.add_resource(PostgresQueryHandlerSC2, '/postgres/sc2/', resource_class_kwargs={'cur': cur_postgres})  # add class property 'cur'
api.add_resource(PostgresQueryHandlerSC3, '/postgres/sc3/', resource_class_kwargs={'cur': cur_postgres})  # add class property 'cur'
api.add_resource(PostgresQueryHandlerSC4, '/postgres/sc4/', resource_class_kwargs={'cur': cur_postgres})  # add class property 'cur'
api.add_resource(PostgresQueryHandlerSC5, '/postgres/sc5/', resource_class_kwargs={'cur': cur_postgres})  # add class property 'cur'

# Mongo
api.add_resource(MongoQueryHandlerSC1, '/mongo/sc1/', resource_class_kwargs={'db': db_mongo})  # add class property 'db'
api.add_resource(MongoQueryHandlerSC2, '/mongo/sc2/', resource_class_kwargs={'db': db_mongo})  # add class property 'db'
api.add_resource(MongoQueryHandlerSC3, '/mongo/sc3/', resource_class_kwargs={'db': db_mongo})  # add class property 'db'
api.add_resource(MongoQueryHandlerSC4, '/mongo/sc4/', resource_class_kwargs={'db': db_mongo})  # add class property 'db'
api.add_resource(MongoQueryHandlerSC5, '/mongo/sc5/', resource_class_kwargs={'db': db_mongo})  # add class property 'db'

# Neo4j
api.add_resource(Neo4jQueryHandler, '/neo4j', resource_class_kwargs={'session': session_neo4j})  # add class property 'session'

# Run the service
app.run(host='localhost', port=9000)