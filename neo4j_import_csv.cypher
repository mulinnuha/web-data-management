// Create acted_in
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:/path/to/acted_in.csv" AS row
CREATE (:acted_in {idacted_in: row.idacted_in, idmovies: row.idmovies, idseries: row.idseries, idactors: row.idactors, character: row.character, billing_position: row.billing_position});

// Create actors
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:/path/to/actors.csv" AS row
CREATE (:actors {idactors: row.idactors, lname: row.lname, fname: row.fname, mname: row.mname, gender: row.gender, number: row.number});

// Create aka_names
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:/path/to/aka_names.csv" AS row
CREATE (:aka_names {idaka_names: row.idaka_names, idactors: row.idactors, name: row.name});

// Create aka_titles
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:/path/to/aka_titles.csv" AS row
CREATE (:aka_titles {idaka_titles:row.idaka_titles, idmovies: row.idmovies, title: row.title, location: row.location, year: row.year});

// Create genres
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:/path/to/genres.csv" AS row
CREATE (:genres {idgenres: row.idgenres, genre: row.genre});

// Create keywords
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:/path/to/keywords.csv" AS row
CREATE (:keywords {idkeywords: row.idkeywords, keyword: row.keyword});

// Create movies
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:/path/to/movies.csv" AS row
CREATE (:movies {idmovies: row.idmovies, title: row.title, year: row.year, number: row.number, type: row.type, location: row.location, language: row.language});

// Create movies_genres
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:/path/to/movies_genres.csv" AS row
CREATE (:movies_genres {idmovies_genres: row.idmovies_genres, idmovies: row.idmovies, idgenres: row.idgenres, idseries: row.idseries});

// Create movies_keywords
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:/path/to/movies_keywords.csv" AS row
CREATE (:movies_keywords {idmovies_keywords: row.idmovies_keywords, idmovies: row.idmovies, idkeywords: row.idkeywords, idseries: row.idseries});

// Create series
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:/path/to/series.csv" AS row
CREATE (:series {idseries: row.idseries, idmovies: row.idmovies, name: row.name, season: row.season, number: row.number});

CREATE INDEX ON :acted_in(idacted_in);
CREATE INDEX ON :actors(idactors);
CREATE INDEX ON :actors(fname);
CREATE INDEX ON :aka_names(idaka_names);
CREATE INDEX ON :aka_titles(idaka_titles);
CREATE INDEX ON :genres(idgenres);
CREATE INDEX ON :keywords(idkeywords);
CREATE INDEX ON :movies(idmovies);
CREATE INDEX ON :movies(title);
CREATE INDEX ON :movies_genres(idmovies_genres);
CREATE INDEX ON :movies_keywords(idmovies_keywords);
CREATE INDEX ON :series(idseries);

schema await

// Create relationships acted_in to actors, movies, and series
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:/path/to/acted_in.csv" AS row
MATCH (acted_in:acted_in {idacted_in: row.idacted_in})
MATCH (actors:actors {idactors: row.idactors})
MERGE (acted_in)-[pu:ACTORS]->(actors);

USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:/path/to/acted_in.csv" AS row
MATCH (acted_in:acted_in {idacted_in: row.idacted_in})
MATCH (movies:movies {idmovies: row.idmovies})
MERGE (acted_in)-[pu:MOVIES]->(movies);

USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:/path/to/acted_in.csv" AS row
MATCH (acted_in:acted_in {idacted_in: row.idacted_in})
MATCH (series:series {idseries: row.idseries})
MERGE (acted_in)-[pu:SERIES]->(series);

// Create relationships between aka_names and actors
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:/path/to/aka_names.csv" AS row
MATCH (aka_names:aka_names {idaka_names: row.idaka_names})
MATCH (actors:actors {idactors: row.idactors})
MERGE (aka_names)-[pu:ACTORS]->(actors);

// Create relationships between series and movies
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:/path/to/series.csv" AS row
MATCH (series:series {idseries: row.idseries})
MATCH (movies:movies {idmovies: row.idmovies})
MERGE (series)-[pu:MOVIES]->(movies);

// Create relationships between aka_titles and movies
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:/path/to/aka_titles.csv" AS row
MATCH (aka_titles:aka_titles {idaka_titles: row.idaka_titles})
MATCH (movies:movies {idmovies: row.idmovies})
MERGE (aka_titles)-[pu:MOVIES]->(movies);

// Create relationships movie_genres to movies, series, and genres
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:/path/to/movies_genres.csv" AS row
MATCH (movies_genres:movies_genres {idmovies_genres: row.idmovies_genres})
MATCH (movies:movies {idmovies: row.idmovies})
MERGE (movies_genres)-[pu:MOVIES]->(movies);

USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:/path/to/movies_genres.csv" AS row
MATCH (movies_genres:movies_genres {idmovies_genres: row.idmovies_genres})
MATCH (series:series {idseries: row.idseries})
MERGE (movies_genres)-[pu:SERIES]->(series);

USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:/path/to/movies_genres.csv" AS row
MATCH (movies_genres:movies_genres {idmovies_genres: row.idmovies_genres})
MATCH (genres:genres {idgenres: row.idgenres})
MERGE (movies_genres)-[pu:GENRES]->(genres);

// Create relationships movies_keywords to movies, series, and keywords
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:/path/to/movies_keywords.csv" AS row
MATCH (movies_keywords:movies_keywords {idmovies_keywords: row.idmovies_keywords})
MATCH (movies:movies {idmovies: row.idmovies})
MERGE (movies_keywords)-[pu:MOVIES]->(movies);

USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:/path/to/movies_keywords.csv" AS row
MATCH (movies_keywords:movies_keywords {idmovies_keywords: row.idmovies_keywords})
MATCH (series:series {idseries: row.idseries})
MERGE (movies_keywords)-[pu:SERIES]->(series);

USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:/path/to/movies_keywords.csv" AS row
MATCH (movies_keywords:movies_keywords {idmovies_keywords: row.idmovies_keywords})
MATCH (keywords:keywords {idkeywords: row.idkeywords})
MERGE (movies_keywords)-[pu:KEYWORDS]->(keywords);