import itertools


def lists_with_labels(lists, labels):
    """
    List of lists to List of dictionaries with labels
    """
    output_list = []

    for list in lists:
        dict = {}
        for i in range(len(labels)):
            label = labels[i]
            item = list[i]
            dict[label] = item if item is not None else ''
        output_list.append(dict)

    return output_list


# Example
# print lists_with_labels([[2, 'A'],
#                          [4, 'B'],
#                          [6, 'C']],
#                         ['id', 'name'])


def merge_list(input_list):
    return list(itertools.chain(*input_list))


# Example
# print merge_list([[1, 2], [3, 4, 5], [6]])


def uniquify_list(l):
    return list(set(l))


# Example
# print uniquify_list([3, 3, 5, 6])


def merge_mongo_dicts(dicts):
    """
    Combine dicts and remove "_id" field
    """
    result = {}
    for dict in dicts:
        for k, v in dict.iteritems():
            result[k] = v
    if "_id" in result.keys():
        del result["_id"]
    return result


def remove_empty_strings(str_list):
    str_list = list(filter(None, str_list))
    return str_list


# Example
# print remove_empty_strings(["", "", "3", "5"])


def merge_json_files(outfile, infiles):
    file(outfile, "w") \
        .write("[%s]" % (",".join([mangle(file(f).read()) for f in infiles])))


def mangle(s):
    return s.strip()[1:-1]


# Example
# merge_json_files('movies.json', ['movies_1.json',
#                                  'movies_2.json',
#                                  'movies_3.json'])


def get_one(mongo_cursor):
    for doc in mongo_cursor:
        return doc
    return None


def get_all(mongo_cursor):
    docs = []
    for doc in mongo_cursor:
        docs.append(doc)
    return docs


def merge_and_remove_labels(dicts, labels):
    result = {}
    for dict in dicts:
        for k, v in dict.iteritems():
            result[k] = v

    for label in labels:
        if label in result.keys():
            del result[label]
    return result


def remove_labels(dict, labels):
    for label in labels:
        if label in dict.keys():
            del dict[label]
    return dict
