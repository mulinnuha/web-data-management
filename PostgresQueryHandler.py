from flask_restful import Resource
from flask import request, jsonify
from psycopg2.extensions import AsIs

from util import lists_with_labels, merge_list, uniquify_list


class PostgresQueryHandlerSC1(Resource):  # extends Resource
    """
    Handles SC1 queries
    Example GET requests:
    1) localhost:9000/postgres/sc1?id=3
    2) localhost:9000/postgres/sc1?title=Please like me
    3) localhost:9000/postgres/sc1?title=Please Li&year=2013
    """

    def __init__(self, cur):
        self.cur = cur

    # GET request function
    def get(self):
        id_given = request.args.get('id')  # id variable given by the user in the GET post
        title_given = request.args.get('title')  # title variable given by the user in the GET post
        year_given = request.args.get('year')

        valid_year_given = year_given is not None and year_given.isdigit()

        if (id_given is None or not id_given.isdigit()) and title_given is None:
            return jsonify({'results': []})  # empty list

        # Movie info
        q_movie_info = """ SELECT m.idmovies, m.title, m.year, m.type
                           FROM movies AS m
                           WHERE m.idmovies = %(id)s """

        # Movie series names
        q_movie_series_names = """ SELECT s.name
                                   FROM series AS s
                                   WHERE s.idmovies = %(id)s """

        # Movie genres
        q_movie_genre_labels = """ SELECT DISTINCT g.genre
                                   FROM genres AS g
                                   JOIN movies_genres AS mg
                                   ON g.idgenres = mg.idgenres
                                   WHERE mg.idmovies = %(id)s """  # Might need DISTINCT

        # Movie keywords
        q_movie_keywords = """ SELECT DISTINCT k.keyword
                               FROM keywords AS k
                               JOIN movies_keywords AS mk
                               ON k.idkeywords = mk.idkeywords
                               WHERE mk.idmovies = %(id)s """  # DISTINCT because it has multiple entries...

        # Movie actors
        q_movie_actors_info = """ SELECT a.fname, a.lname, a.gender, ai.character, MIN(ai.billing_position)
                                  FROM actors AS a
                                  JOIN acted_in AS ai
                                  ON a.idactors = ai.idactors
                                  WHERE ai.idmovies = %(id)s
                                  GROUP BY a.fname, a.lname, a.gender, ai.character
                                  ORDER BY MIN(ai.billing_position) """  # DISTINCT because it has multiple entries...

        ids = []

        # If user gave a title, then find a list of movie ids
        if title_given is not None:
            q = """ SELECT m.idmovies
                    FROM movies AS m
                    WHERE LOWER(m.title) LIKE LOWER(%(title)s) %(year_where_clause)s"""

            year_where_clause = ''
            if valid_year_given:
                year_where_clause = 'AND m.year=' + year_given

            self.cur.execute(q, {'title': '%' + title_given + '%',
                                 'year_where_clause': AsIs(year_where_clause)})
            rows = self.cur.fetchall()
            ids_from_title = merge_list(rows)
            ids += ids_from_title

        # If user gave an id and it is an integer
        if id_given is not None and id_given.isdigit():
            id_given = int(id_given)
            ids.append(id_given)

        results_list = []

        # For every unique movie id
        for id in uniquify_list(ids):
            result_dict = {}

            # Movie info
            self.cur.execute(q_movie_info, {'id': id})
            rows = self.cur.fetchall()

            if rows:  # if list is not empty
                row = rows[0]  # only 1 row output, since ids are unique

                # Hacky code... yet fairly efficient
                title = row[1]
                year = row[2]
                type = row[3]
                # For partial title matches we want to restrict to "movies" only (type==3)
                partial_match = id != id_given and title != title_given
                if partial_match and type != 3:
                    continue  # don't show this movie result because type!=3
                # Year filtering for id_given
                if id == id_given and valid_year_given and year != year_given:
                    continue  # don't show this movie result because year does not match with year_given

                result_dict['id'] = row[0]
                result_dict['title'] = row[1]
                result_dict['year'] = row[2]

            # Movie series names
            self.cur.execute(q_movie_series_names, {'id': id})
            rows = self.cur.fetchall()
            result_dict['serie_names'] = merge_list(rows)

            # Movie genres
            self.cur.execute(q_movie_genre_labels, {'id': id})
            rows = self.cur.fetchall()
            result_dict['genres'] = merge_list(rows)

            # Movie keywords
            self.cur.execute(q_movie_keywords, {'id': id})
            rows = self.cur.fetchall()
            result_dict['keywords'] = merge_list(rows)

            # Movie actors
            self.cur.execute(q_movie_actors_info, {'id': id})
            rows = self.cur.fetchall()
            result_dict['actors'] = lists_with_labels(rows, ['first_name', 'last_name', 'gender', 'role'])

            results_list.append(result_dict)

        return jsonify({'results': results_list})


class PostgresQueryHandlerSC2(Resource):  # extends Resource
    """
    Handles SC2 queries
    Example GET requests:
    1) localhost:9000/postgres/sc2?id=9
    2) localhost:9000/postgres/sc2?fname=Lucienne
    3) localhost:9000/postgres/sc2?lname=Bruinooge
    4) localhost:9000/postgres/sc2?fname=Lucienne&lname=Bruinooge
    """

    def __init__(self, cur):
        self.cur = cur

    # GET request function
    def get(self):
        id = request.args.get('id')
        f_name = request.args.get('fname')
        l_name = request.args.get('lname')

        if (id is None or not id.isdigit()) and (f_name is None) and (l_name is None):
            return jsonify({'results': []})  # empty list

        # Actor info
        q_actor_info = """ SELECT a.fname, a.lname, a.gender
                           FROM actors AS a
                           WHERE a.idactors = %(id)s """

        # Movies played in
        q_movies_played_in = """ SELECT m.title, m.year
                                 FROM movies AS m
                                 JOIN acted_in AS ai
                                 ON m.idmovies = ai.idmovies
                                 WHERE ai.idactors = %(id)s AND m.type = 3
                                 GROUP BY m.idmovies
                                 ORDER BY m.year """

        ids = []

        # If user gave some name, then find a list of movie ids
        if f_name is not None or l_name is not None:
            if f_name is not None:
                if l_name is not None:
                    q = """ SELECT a.idactors
                            FROM actors AS a
                            WHERE LOWER(a.fname) = LOWER(%(f_name)s) AND LOWER(a.lname) = LOWER(%(l_name)s) """
                    self.cur.execute(q, {'f_name': f_name,
                                         'l_name': l_name})
                else:
                    q = """ SELECT a.idactors
                            FROM actors AS a
                            WHERE LOWER(a.fname) = LOWER(%(f_name)s) """
                    self.cur.execute(q, {'f_name': f_name})
            else:
                q = """ SELECT a.idactors
                        FROM actors AS a
                        WHERE LOWER(a.lname) = LOWER(%(l_name)s) """
                self.cur.execute(q, {'l_name': l_name})
            rows = self.cur.fetchall()
            ids_from_name = merge_list(rows)
            ids += ids_from_name

        # If user gave an id and it is an integer
        if id is not None and id.isdigit():
            id = int(id)
            ids.append(id)

        results_list = []

        # For every unique movie id
        for id in uniquify_list(ids):
            result_dict = {}

            # Actor info
            self.cur.execute(q_actor_info, {'id': id})
            rows = self.cur.fetchall()
            if rows:  # if list is not empty
                row = rows[0]  # only 1 row output, since ids are unique
                result_dict['first_name'] = row[0]
                result_dict['last_name'] = row[1]
                result_dict['gender'] = row[2]

            # Movies played in
            self.cur.execute(q_movies_played_in, {'id': id})
            rows = self.cur.fetchall()
            result_dict['movies_played_in'] = lists_with_labels(rows, ['title', 'year'])

            results_list.append(result_dict)

        return jsonify({'results': results_list})


class PostgresQueryHandlerSC3(Resource):  # extends Resource
    """
    Handles SC3 queries
    Example GET requests:
    1) localhost:9000/postgres/sc3?id=9
    2) localhost:9000/postgres/sc3?fname=Lucienne
    3) localhost:9000/postgres/sc3?lname=Bruinooge
    4) localhost:9000/postgres/sc3?fname=Lucienne&lname=Bruinooge
    """

    def __init__(self, cur):
        self.cur = cur

    # GET request function
    def get(self):
        id = request.args.get('id')
        f_name = request.args.get('fname')
        l_name = request.args.get('lname')

        if (id is None or not id.isdigit()) and (f_name is None) and (l_name is None):
            return jsonify({'results': []})  # empty list

        # Actor info
        q_actor_info = """ SELECT a.fname, a.lname, a.gender
                               FROM actors AS a
                               WHERE a.idactors = %(id)s """

        # Movies played in
        q_movies_played_in = """ SELECT COUNT(DISTINCT m.idmovies)
                                 FROM movies AS m
                                 JOIN acted_in AS ai
                                 ON m.idmovies = ai.idmovies
                                 WHERE ai.idactors = %(id)s AND m.type = 3
                                 GROUP BY ai.idactors """

        ids = []

        # If user gave some name, then find a list of movie ids
        if f_name is not None or l_name is not None:
            if f_name is not None:
                if l_name is not None:
                    q = """ SELECT a.idactors
                                FROM actors AS a
                                WHERE LOWER(a.fname) = LOWER(%(f_name)s) AND LOWER(a.lname) = LOWER(%(l_name)s) """
                    self.cur.execute(q, {'f_name': f_name,
                                         'l_name': l_name})
                else:
                    q = """ SELECT a.idactors
                                FROM actors AS a
                                WHERE LOWER(a.fname) = LOWER(%(f_name)s) """
                    self.cur.execute(q, {'f_name': f_name})
            else:
                q = """ SELECT a.idactors
                            FROM actors AS a
                            WHERE LOWER(a.lname) = LOWER(%(l_name)s) """
                self.cur.execute(q, {'l_name': l_name})
            rows = self.cur.fetchall()
            ids_from_name = merge_list(rows)
            ids += ids_from_name

        # If user gave an id and it is an integer
        if id is not None and id.isdigit():
            id = int(id)
            ids.append(id)

        results_list = []

        # For every unique movie id
        for id in uniquify_list(ids):
            result_dict = {}

            # Actor info
            self.cur.execute(q_actor_info, {'id': id})
            rows = self.cur.fetchall()
            if rows:  # if list is not empty
                row = rows[0]  # only 1 row output, since ids are unique
                result_dict['first_name'] = row[0]
                result_dict['last_name'] = row[1]
                result_dict['gender'] = row[2]

            # Movies played in
            self.cur.execute(q_movies_played_in, {'id': id})
            rows = self.cur.fetchall()

            result_dict['movies_played_in'] = 0
            for row in rows:
                result_dict['movies_played_in'] = row[0]  # 1 value because of COUNT(*)

            results_list.append(result_dict)

        return jsonify({'results': results_list})


class PostgresQueryHandlerSC4(Resource):  # extends Resource
    """
    Handles SC4 queries
    Example GET requests:
    1) localhost:9000/postgres/sc4?genre=Drama&year=1975
    2) localhost:9000/postgres/sc4?genre=Drama&year=1975&endyear=1990
    """

    def __init__(self, cur):
        self.cur = cur

    # GET request function
    def get(self):
        genre_given = request.args.get('genre')
        year_given = request.args.get('year')
        end_year_given = request.args.get('endyear')

        if (genre_given is None) or (year_given is None):
            return jsonify({'results': []})  # empty list

        # Movie info
        if end_year_given is not None and end_year_given.isdigit():
            q_movie_info = """ SELECT m.idmovies, m.title, m.year
                               FROM movies AS m
                               JOIN movies_genres AS mg
                               ON m.idmovies = mg.idmovies
                               JOIN genres AS g
                               ON g.idgenres = mg.idgenres
                               WHERE LOWER(g.genre) = LOWER(%(genre_given)s) AND m.year BETWEEN %(year_given)s AND %(end_year_given)s AND m.type = 3
                               ORDER BY m.year, m.title """
        else:
            q_movie_info = """ SELECT m.idmovies, m.title, m.year
                               FROM movies AS m
                               JOIN movies_genres AS mg
                               ON m.idmovies = mg.idmovies
                               JOIN genres AS g
                               ON g.idgenres = mg.idgenres
                               WHERE LOWER(g.genre) = LOWER(%(genre_given)s) AND m.year=%(year_given)s AND m.type = 3
                               ORDER BY m.year, m.title """
        self.cur.execute(q_movie_info, {'genre_given': genre_given,
                                        'year_given': year_given,
                                        'end_year_given': end_year_given})
        rows = self.cur.fetchall()

        results_list = []

        for row in rows:
            result_dict = {'id': row[0],
                           'title': row[1],
                           'year': row[2]}

            results_list.append(result_dict)

        return jsonify({'results': results_list})


class PostgresQueryHandlerSC5(Resource):  # extends Resource
    """
    Handles SC5 queries
    Example GET requests:
    1) localhost:9000/postgres/sc5?year=1975
    2) localhost:9000/postgres/sc5?year=1975&endyear=1980
    """

    def __init__(self, cur):
        self.cur = cur

    # GET request function
    def get(self):
        year_given = request.args.get('year')
        end_year_given = request.args.get('endyear')

        if year_given is None:
            return jsonify({'results': []})  # empty list

        # Genre info
        if end_year_given is not None and end_year_given.isdigit():
            q_genre_info = """ SELECT DISTINCT g.genre, COUNT(mg.idmovies)
                               FROM movies AS m
                               JOIN movies_genres AS mg
                               ON m.idmovies = mg.idmovies
                               JOIN genres AS g
                               ON g.idgenres = mg.idgenres
                               WHERE m.year BETWEEN %(year_given)s AND %(end_year_given)s AND m.type = 3
                               GROUP BY g.genre
                               ORDER BY g.genre """
        else:
            q_genre_info = """ SELECT g.genre, COUNT(mg.idmovies)
                               FROM movies AS m
                               JOIN movies_genres AS mg
                               ON m.idmovies = mg.idmovies
                               JOIN genres AS g
                               ON g.idgenres = mg.idgenres
                               WHERE m.year=%(year_given)s AND m.type = 3
                               GROUP BY g.genre
                               ORDER BY g.genre """

        self.cur.execute(q_genre_info, {'year_given': year_given,
                                        'end_year_given': end_year_given})
        rows = self.cur.fetchall()

        results_list = []

        for row in rows:
            result_dict = {'genre': row[0],
                           'num_of_movies': row[1]}

            results_list.append(result_dict)

        return jsonify({'results': results_list})
