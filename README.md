# Group 14 - Web Data Management

## Installation
Using terminal/command prompt:

```
pip install virtualenv
virtualenv -p python2.7 webdata
cd webdata
(copy the "wdm" folder containing the code here)
source bin/activate
cd wdm
pip install -r requirements.txt
```

## Data Preprocessing
### PostgreSQL

**Import**

- Restore into PostgreSQL using `pgsql_backup.backup` file
- Alternative: import the latest IMDB data using script from [https://github.com/ameerkat/imdb-to-sql](https://github.com/ameerkat/imdb-to-sql)

### MongoDB

**Export**

- Uncomment the last three lines in `mongo_init.py`
- Change the connection string (host, port, dbname, username, password) for Postgres and MongoDB accordingly
- Run `python mongo_init.py`
- The output is two JSON files: `movies.json` and `actors.json`

**Import**

- Open `mongo-shell`
- Run 
```
mongoimport --db (database_name) --collection movies --file /path/to/movies.json --jsonArray
mongoimport --db (database_name) --collection actors --file /path/to/actors.json --jsonArray
```
- Before running the above script, change the (database_name) and file path of `movies.json` and `actors.json` accordingly

### Neo4j

**Export**

- Open terminal/command line
- Run `psql -U (user_postgres) -d (database_name) < neo4j_export_csv.sql` 
- Before running the script above, change the (user_postgres) and (database_name) according to your PostgreSQL configuration
- Also change the directory of the output in `neo4j_export_csv.sql`
- The output is 10 CSV files located in the specified directory in `neo4j_export_csv.sql`

**Import**

- Open terminal/command line
- Run `neo4j-shell -path <NEO4JHOME>/data/databases/imdb_data.db -file neo4j_import_csv.cypher ERROR -v`
- Change the `<NEO4JHOME>` to the installation directory of NEO4J
- Before running the script, find and replace the directory name (file:/path/to/) in `neo4j_import_csv.cypher` into the location of CSV files from export step
- Change the configuration `neo4j.conf` line `dbms.active_database=graph.db` into `dbms.active_database=imdb_data.db`
- Restart Neo4j

## Running the program
- Change the port configuration in the last line of `app.py` if necessary (the default is 9000)
- Run `python app.py`


## Access the webservice with REST client

### PostgreSQL
```
GET http://localhost:9000/postgres/sc1?{param}
GET http://localhost:9000/postgres/sc2?{param}
...
GET http://localhost:9000/postgres/sc5?{param}
```

Example: `GET http://localhost:9000/postgres/sc1?title=Star Wars`

### MongoDB
```
GET http://localhost:9000/mongo/sc1?{param}
GET http://localhost:9000/mongo/sc2?{param}
...
GET http://localhost:9000/mongo/sc5?{param}
```

Example: `GET http://localhost:9000/mongo/sc1?title=Star Wars`

### Neo4j
```
GET http://localhost:9000/neo4j?sc=1&{param}
GET http://localhost:9000/neo4j?sc=2&{param}
...
GET http://localhost:9000/neo4j?sc=5&{param}
```

Example: `GET http://localhost:9000/neo4j?sc=1&title=Star Wars`
