from flask_restful import Resource
from flask import request, jsonify
import re
from util import get_one, get_all, remove_labels


class MongoQueryHandlerSC1(Resource):
    """
    Handles SC1 queries
    Example GET requests:
    1) localhost:9000/mongo/sc1?id=3
    2) localhost:9000/mongo/sc1?title=Please like me
    3) localhost:9000/mongo/sc1?title=Please Li&year=2013
    """

    def __init__(self, db):
        self.db = db

    # GET request function
    def get(self):
        id_given = request.args.get('id')  # id variable given by the user in the GET post
        title_given = request.args.get('title')  # title variable given by the user in the GET post
        year_given = request.args.get('year')

        valid_year_given = year_given is not None and year_given.isdigit()
        valid_id_given = id_given is not None and id_given.isdigit()
        valid_title_given = title_given is not None

        if valid_id_given:
            id_given = int(id_given)

            # Movie info, series names
            c1 = self.db.movies.aggregate([{"$match": {"idmovies": id_given}},
                                           {"$project": {'_id': 0,
                                                         'id': "$idmovies",
                                                         'title': '$title',
                                                         'year': '$year',
                                                         'genres': '$genres',
                                                         'keywords': '$keywords',
                                                         'serie_names': '$series.name',
                                                         'actor_ids': '$actor_ids'
                                                         }}])
            movie_doc = get_one(c1)  # get result doc

            if movie_doc is None:
                return jsonify({'results': []})  # empty list

            c2 = self.db.actors.aggregate([{'$unwind': "$acted_in"},
                                           {"$match": {"idactors": {'$in': movie_doc['actor_ids']},
                                                       "acted_in.idmovies": id_given}},
                                           {"$group": {"_id": "$idactors",
                                                       'first_name': {'$first': '$fname'},
                                                       'last_name': {'$first': '$lname'},
                                                       'gender': {'$first': '$gender'},
                                                       'bp': {'$min': '$acted_in.billing_position'},
                                                       'roles': {'$addToSet': '$acted_in.character'}}},
                                           {'$sort': {'bp': 1}},
                                           {"$project": {'_id': 0,
                                                         'bp': 0}}])
            actor_docs = get_all(c2)  # get result docs

            if len(actor_docs) > 0:
                movie_doc['actors'] = actor_docs

            # Format result doc
            result = remove_labels(movie_doc, ['actor_ids'])

            return jsonify({'results': [result]})

        elif valid_title_given:
            title_given = str(title_given)

            if valid_year_given:
                year_given = int(year_given)

                c1 = self.db.movies.aggregate(
                    [{"$match": {'title': {'$regex': re.compile(".*" + title_given + ".*", re.IGNORECASE)},
                                 'year': year_given}},
                     {"$project": {'_id': 0,
                                   'id': "$idmovies",
                                   'title': '$title',
                                   'year': '$year',
                                   'genres': '$genres',
                                   'keywords': '$keywords',
                                   'serie_names': '$series.name',
                                   'actor_ids': '$actor_ids',
                                   'type': '$type'
                                   }}])
            else:
                c1 = self.db.movies.aggregate(
                    [{"$match": {'title': {'$regex': re.compile(".*" + title_given + ".*", re.IGNORECASE)}}},
                     {"$project": {'_id': 0,
                                   'id': "$idmovies",
                                   'title': '$title',
                                   'year': '$year',
                                   'genres': '$genres',
                                   'keywords': '$keywords',
                                   'serie_names': '$series.name',
                                   'actor_ids': '$actor_ids',
                                   'type': '$type'
                                   }}])
            movie_docs = get_all(c1)  # get result docs

            results = []
            for movie_doc in movie_docs:
                partial_match = movie_doc['title'] != title_given

                # If partial match, we want to restrict to movies only (type==3)
                if partial_match:
                    if movie_doc['type'] != 3:
                        continue  # skip non-movie

                c2 = self.db.actors.aggregate([{'$unwind': "$acted_in"},
                                               {"$match": {"idactors": {'$in': movie_doc['actor_ids']},
                                                           "acted_in.idmovies": int(movie_doc['id'])}},
                                               {"$group": {"_id": "$idactors",
                                                           'first_name': {'$first': '$fname'},
                                                           'last_name': {'$first': '$lname'},
                                                           'gender': {'$first': '$gender'},
                                                           'bp': {'$min': '$acted_in.billing_position'},
                                                           'roles': {'$addToSet': '$acted_in.character'}}},
                                               {'$sort': {'bp': 1}},
                                               {"$project": {'_id': 0,
                                                             'bp': 0}}])
                actor_docs = get_all(c2)  # get result docs

                if len(actor_docs) > 0:
                    movie_doc['actors'] = actor_docs

                # Format result doc
                result = remove_labels(movie_doc, ['actor_ids', 'type'])
                results.append(result)

            return jsonify({'results': results})

        return jsonify({'results': []})  # empty list


class MongoQueryHandlerSC2(Resource):  # extends Resource
    """
    Handles SC2 queries
    Example GET requests:
    1) localhost:9000/mongo/sc2?id=9
    2) localhost:9000/mongo/sc2?fname=Lucienne
    3) localhost:9000/mongo/sc2?lname=Bruinooge
    4) localhost:9000/mongo/sc2?fname=Lucienne&lname=Bruinooge
    """

    def __init__(self, db):
        self.db = db

    # GET request function
    def get(self):
        id_given = request.args.get('id')
        f_name = request.args.get('fname')
        l_name = request.args.get('lname')

        valid_id_given = id_given is not None and id_given.isdigit()
        valid_f_name_given = f_name is not None
        valid_l_name_given = l_name is not None
        valid_name_given = valid_f_name_given or valid_l_name_given

        if valid_id_given:
            id_given = int(id_given)

            c1 = self.db.actors.aggregate([{"$match": {'idactors': id_given}},
                                           {"$project": {'_id': 0,
                                                         'first_name': '$fname',
                                                         'last_name': '$lname',
                                                         'gender': '$gender',
                                                         'movie_ids': '$acted_in.idmovies'}}])
            actor_doc = get_one(c1)  # get result doc

            if actor_doc is None:
                return jsonify({'results': []})  # empty list

            c2 = self.db.movies.aggregate([{"$match": {'idmovies': {'$in': actor_doc['movie_ids']},
                                                       'type': 3}},
                                           {"$project": {'_id': 0,
                                                         'title': '$title',
                                                         'year': '$year'}},
                                           {'$sort': {'year': 1}}])
            movie_docs = get_all(c2)

            if len(movie_docs) > 0:
                actor_doc['movies_played_in'] = movie_docs

            # Format result doc
            result = remove_labels(actor_doc, ['movie_ids'])

            return jsonify({'results': [result]})
        elif valid_name_given:
            f_name = str(f_name)
            l_name = str(l_name)

            if valid_f_name_given and valid_l_name_given:
                c1 = self.db.actors.aggregate([{"$match": {'fname': f_name,
                                                           'lname': l_name}},
                                               {"$project": {'_id': 0,
                                                             'first_name': '$fname',
                                                             'last_name': '$lname',
                                                             'gender': '$gender',
                                                             'movie_ids': '$acted_in.idmovies'}}])
            elif valid_f_name_given:
                c1 = self.db.actors.aggregate([{"$match": {'fname': f_name}},
                                               {"$project": {'_id': 0,
                                                             'first_name': '$fname',
                                                             'last_name': '$lname',
                                                             'gender': '$gender',
                                                             'movie_ids': '$acted_in.idmovies'}}])
            elif valid_l_name_given:
                c1 = self.db.actors.aggregate([{"$match": {'lname': l_name}},
                                               {"$project": {'_id': 0,
                                                             'first_name': '$fname',
                                                             'last_name': '$lname',
                                                             'gender': '$gender',
                                                             'movie_ids': '$acted_in.idmovies'}}])
            actor_docs = get_all(c1)  # get result doc

            results = []
            for actor_doc in actor_docs:
                c2 = self.db.movies.aggregate([{"$match": {'idmovies': {'$in': actor_doc['movie_ids']},
                                                           'type': 3}},
                                               {"$project": {'_id': 0,
                                                             'title': '$title',
                                                             'year': '$year'}},
                                               {'$sort': {'year': 1}}])
                movie_docs = get_all(c2)

                if len(movie_docs) > 0:
                    actor_doc['movies_played_in'] = movie_docs

                # Format result doc
                result = remove_labels(actor_doc, ['movie_ids'])
                results.append(result)

            return jsonify({'results': results})

        return jsonify({'results': []})


class MongoQueryHandlerSC3(Resource):  # extends Resource
    """
    Handles SC3 queries
    Example GET requests:
    1) localhost:9000/mongo/sc3?id=9
    2) localhost:9000/mongo/sc3?fname=Lucienne
    3) localhost:9000/mongo/sc3?lname=Bruinooge
    4) localhost:9000/mongo/sc3?fname=Lucienne&lname=Bruinooge
    """

    def __init__(self, db):
        self.db = db

    # GET request function
    def get(self):
        id_given = request.args.get('id')
        f_name = request.args.get('fname')
        l_name = request.args.get('lname')

        valid_id_given = id_given is not None and id_given.isdigit()
        valid_f_name_given = f_name is not None
        valid_l_name_given = l_name is not None
        valid_name_given = valid_f_name_given or valid_l_name_given

        if valid_id_given:
            id_given = int(id_given)

            c1 = self.db.actors.aggregate([{"$match": {'idactors': id_given}},
                                           {"$project": {'_id': 0,
                                                         'first_name': '$fname',
                                                         'last_name': '$lname',
                                                         'gender': '$gender',
                                                         'movie_ids': '$acted_in.idmovies'}}])
            actor_doc = get_one(c1)  # get result doc

            if actor_doc is None:
                return jsonify({'results': []})  # empty list

            c2 = self.db.movies.aggregate(
                [{"$match": {'idmovies': {'$in': actor_doc['movie_ids']},
                             'type': 3}},
                 {"$group": {'_id': None,
                             'movies_played_in': {'$sum': 1}}},
                 {"$project": {'_id': 0}}])
            movie_info_doc = get_one(c2)

            # Format result
            if movie_info_doc is not None:
                actor_doc['movies_played_in'] = movie_info_doc['movies_played_in']
            actor_doc = remove_labels(actor_doc, ['movie_ids'])

            return jsonify({'results': [actor_doc]})
        elif valid_name_given:
            f_name = str(f_name)
            l_name = str(l_name)

            if valid_f_name_given and valid_l_name_given:
                c1 = self.db.actors.aggregate([{"$match": {'fname': f_name,
                                                           'lname': l_name}},
                                               {"$project": {'_id': 0,
                                                             'first_name': '$fname',
                                                             'last_name': '$lname',
                                                             'gender': '$gender',
                                                             'movie_ids': '$acted_in.idmovies'}}])
            elif valid_f_name_given:
                c1 = self.db.actors.aggregate([{"$match": {'fname': f_name}},
                                               {"$project": {'_id': 0,
                                                             'first_name': '$fname',
                                                             'last_name': '$lname',
                                                             'gender': '$gender',
                                                             'movie_ids': '$acted_in.idmovies'}}])
            elif valid_l_name_given:
                c1 = self.db.actors.aggregate([{"$match": {'lname': l_name}},
                                               {"$project": {'_id': 0,
                                                             'first_name': '$fname',
                                                             'last_name': '$lname',
                                                             'gender': '$gender',
                                                             'movie_ids': '$acted_in.idmovies'}}])
            actor_docs = get_all(c1)  # get result doc

            results = []
            for actor_doc in actor_docs:
                c2 = self.db.movies.aggregate([{"$match": {'idmovies': {'$in': actor_doc['movie_ids']},
                                                           'type': 3}},
                                               {"$group": {'_id': None,
                                                           'movies_played_in': {'$sum': 1}}},
                                               {"$project": {'_id': 0}}])
                movie_info_doc = get_one(c2)

                # Format result
                actor_doc['movies_played_in'] = movie_info_doc['movies_played_in'] if movie_info_doc is not None else 0
                actor_doc = remove_labels(actor_doc, ['movie_ids'])

                results.append(actor_doc)

            return jsonify({'results': results})

        return jsonify({'results': []})


class MongoQueryHandlerSC4(Resource):  # extends Resource
    """
    Handles SC4 queries
    Example GET requests:
    1) localhost:9000/mongo/sc4?genre=Drama&year=1975
    2) localhost:9000/mongo/sc4?genre=Drama&year=1975&endyear=1990
    """

    def __init__(self, db):
        self.db = db

    # GET request function
    def get(self):
        genre_given = request.args.get('genre')
        start_year_given = request.args.get('year')
        end_year_given = request.args.get('endyear')

        valid_genre_given = genre_given is not None
        valid_start_year_given = start_year_given is not None and start_year_given.isdigit()
        valid_end_year_given = end_year_given is not None and end_year_given.isdigit()

        if valid_genre_given and valid_start_year_given:
            start_year_given = int(start_year_given)

            if valid_end_year_given:
                end_year_given = int(end_year_given)
            else:
                end_year_given = start_year_given

            genre_given = str(genre_given)

            c1 = self.db.movies.aggregate([{"$match": {'year': {'$gte': start_year_given,
                                                                '$lte': end_year_given},
                                                       'genres': genre_given,
                                                       'type': 3}},  # check if array contains
                                           {"$project": {'_id': 0,
                                                         'id': '$idmovies',
                                                         'title': '$title',
                                                         'year': '$year'}},
                                           {'$sort': {'title': 1,
                                                      'year': 1}}])
            movie_docs = get_all(c1)  # get result doc

            results = []
            for movie_doc in movie_docs:
                results.append(movie_doc)

            return jsonify({'results': results})

        return jsonify({'results': []})


class MongoQueryHandlerSC5(Resource):  # extends Resource
    """
    Handles SC5 queries
    Example GET requests:
    1) localhost:9000/mongo/sc5?year=1975
    2) localhost:9000/mongo/sc5?year=1975&endyear=1980
    """

    def __init__(self, db):
        self.db = db

    # GET request function
    def get(self):
        start_year_given = request.args.get('year')
        end_year_given = request.args.get('endyear')

        valid_start_year_given = start_year_given is not None and start_year_given.isdigit()
        valid_end_year_given = end_year_given is not None and end_year_given.isdigit()

        if valid_start_year_given:
            start_year_given = int(start_year_given)

            if valid_end_year_given:
                end_year_given = int(end_year_given)
            else:
                end_year_given = start_year_given

            c1 = self.db.movies.aggregate([{"$match": {'year': {'$gte': start_year_given,
                                                                '$lte': end_year_given},
                                                       'type': 3}},
                                           {'$unwind': "$genres"},
                                           {"$group": {'_id': '$genres',
                                                       'genre': {'$first': '$genres'},
                                                       'num_of_movies': {'$sum': 1}}},
                                           {'$sort': {'genre': 1}},
                                           {'$project': {'_id': 0}}])
            movie_docs = get_all(c1)  # get result doc

            results = []
            for movie_doc in movie_docs:
                results.append(movie_doc)

            return jsonify({'results': results})

        return jsonify({'results': []})
