from neo4j.v1 import GraphDatabase, basic_auth
from flask_restful import Resource
from flask import request, jsonify
import json

class Neo4jQueryHandler(Resource):  # extends Resource
	def __init__(self, session):
		self.session = session

	# GET request function
	def get(self):
		sc = request.args.get('sc')

		if (sc == "1"):
			"""
		    Handles SC1 queries
		    Example GET requests:
		    1) localhost:9000/neo4j?sc=1&id=3
		    2) localhost:9000/neo4j?sc=1&title=Please like me
		    3) localhost:9000/neo4j?sc=1&title=Please Li&year=2013
		    """
			id = request.args.get('id')  # idmovies given by the user in the GET post
			title = request.args.get('title')  # title given by the user in the GET post
			year = request.args.get('year')  # title given by the user in the GET post

			return jsonify({'results': self.sc1(id, title, year)})
		elif (sc == "2"):
			"""
		    Handles SC2 queries
		    Example GET requests:
		    1) localhost:9000/neo4j?sc=2&id=9
		    2) localhost:9000/neo4j?sc=2&fname=Lucienne
		    3) localhost:9000/neo4j?sc=2&lname=Bruinooge
		    4) localhost:9000/neo4j?sc=2&fname=Lucienne&lname=Bruinooge
		    """
			id = request.args.get('id')  # idactors given by the user in the GET post
			fname = request.args.get('fname')  # fname given by the user in the GET post
			lname = request.args.get('lname')  # lname given by the user in the GET post

			return jsonify({'results': self.sc2(id, fname, lname)})
		elif (sc == "3"):
			"""
		    Handles SC3 queries
		    Example GET requests:
		    1) localhost:9000/neo4j?sc=3&id=9
		    2) localhost:9000/neo4j?sc=3&fname=Lucienne
		    3) localhost:9000/neo4j?sc=3&lname=Bruinooge
		    4) localhost:9000/neo4j?sc=3&fname=Lucienne&lname=Bruinooge
		    """
			id = request.args.get('id')  # idactors given by the user in the GET post
			fname = request.args.get('fname')  # fname given by the user in the GET post
			lname = request.args.get('lname')  # lname given by the user in the GET post

			return jsonify({'results': self.sc3(id, fname, lname)})
		elif (sc == "4"):
			"""
		    Handles SC4 queries
		    Example GET requests:
		    1) localhost:9000/neo4j?sc=4&genre=Drama&start=1975
		    2) localhost:9000/neo4j?sc=4genre=Drama&start=1975&end=1990
		    """
			genre = request.args.get('genre')  # genre given by the user in the GET post
			start = request.args.get('start')  # start year given by the user in the GET post
			end = request.args.get('end')  # end year given by the user in the GET post

			return jsonify({'results': self.sc4(genre, start, end)})
		elif (sc == "5"):
			"""
		    Handles SC5 queries
		    Example GET requests:
		    1) localhost:9000/neo4j?sc=5&start=1975
		    2) localhost:9000/neo4j?sc=5&start=1975&end=1980
		    """
			start = request.args.get('start')  # start year given by the user in the GET post
			end = request.args.get('end')  # end year given by the user in the GET post

			return jsonify({'results': self.sc5(start, end)})

	# GET SC 1
	def sc1(self, idmovies, title, year):
		try:
			# List of movies (SC1) query
			movies_query = '''
				MATCH 
					(n:movies)
				WHERE CASE
					WHEN {idmovies} IS NULL THEN 
						CASE WHEN {year} IS NOT NULL THEN ((n.title =~ ("(?i)" + {title}) OR (n.title =~ ("(?i).*" + {title} + ".*") AND n.type = "3")) AND n.year = {year})
							ELSE (n.title =~ ("(?i)" + {title}) OR (n.title =~ ("(?i).*" + {title} + ".*") AND n.type = "3"))
						END
					ELSE n.idmovies = {idmovies}
				END
				WITH n
				OPTIONAL MATCH
					(n)-[:MOVIES]-(m:acted_in)
				OPTIONAL MATCH
					(m)-[:SERIES]-(o:series)
				WITH n, o.name as series
				WITH n, collect(DISTINCT series) as series
				OPTIONAL MATCH
					(n)-[:MOVIES]-(m:acted_in)-[:ACTORS]-(p:actors)
				WITH n, series, p, m.billing_position as billing, m.character as roles
				WITH n, series, { fname: p.fname, lname: p.lname, gender: p.gender, roles: collect(DISTINCT roles), min_billing: MIN(billing) } as p
				ORDER BY p.min_billing
				WITH n, series, { fname: p.fname, lname: p.lname, gender: p.gender, roles: p.roles } as actors
				WITH n, series, collect(actors) as actors
				OPTIONAL MATCH
					(n)-[:MOVIES]-(q:movies_genres)-[:GENRES]-(r:genres)
				WITH n, series, actors, collect(DISTINCT r.genre) as genres
				OPTIONAL MATCH
					(n)-[:MOVIES]-(q:movies_keywords)-[:KEYWORDS]-(r:keywords)
				WITH n, series, actors, genres, collect(DISTINCT r.keyword) as keywords
				RETURN
				({
					idmovies: n.idmovies, title: n.title, year: n.year, series: series, actors: actors, genres: genres, keywords: keywords
				}) AS Movie
			'''

			results = self.session.run(movies_query, parameters={"title": title, "idmovies": idmovies, "year": year})

			results_list = []
			for record in results:
				results_list.append(record['Movie'])

			return results_list
		except Exception as err: return str(err)

	# GET SC 2
	def sc2(self, idactors, fname, lname):
		if (fname is not None):
			fname = '(?i)' + fname
		if (lname is not None):
			lname = '(?i)' + lname

		try:
			# List of actors (SC2) query
			actors_query = '''
				MATCH 
					(n:actors)
				WHERE CASE
					WHEN {idactors} IS NULL THEN 
						CASE WHEN {fname} IS NULL OR {lname} IS NULL THEN (n.fname =~ {fname} OR n.lname =~ {lname})
							ELSE (n.fname =~ {fname} AND n.lname =~ {lname})
						END
					ELSE n.idactors = {idactors}
				END
				WITH n
				OPTIONAL MATCH
					(n)-[:ACTORS]-(m:acted_in)-[:MOVIES]-(o:movies)
				WITH n,o
				ORDER BY o.year
				WITH n, collect(DISTINCT { title: o.title, year: o.year }) as movies
				RETURN
				({
					fname: n.fname, lname: n.lname, gender: n.gender, movies: movies
				}) AS Actor
			'''

			results = self.session.run(actors_query, parameters={"idactors": idactors, "fname": fname, "lname": lname})

			results_list = []
			for record in results:
				results_list.append(record['Actor'])

			return results_list
		except Exception as err: return str(err)

	# GET SC 3
	def sc3(self, idactors, fname, lname):
		if (fname is not None):
			fname = '(?i)' + fname
		if (lname is not None):
			lname = '(?i)' + lname

		try:
			# Statistic of actors (SC3) query
			actors_query = '''
				MATCH 
					(n:actors)
				WHERE CASE
					WHEN {idactors} IS NULL THEN 
						CASE WHEN {fname} IS NULL OR {lname} IS NULL THEN (n.fname =~ {fname} OR n.lname =~ {lname})
							ELSE (n.fname =~ {fname} AND n.lname =~ {lname})
						END
					ELSE n.idactors = {idactors}
				END
				WITH n
				OPTIONAL MATCH
					(n)-[:ACTORS]-(m:acted_in)-[:MOVIES]-(o:movies)
				WITH n, count(o) as movies
				RETURN
				({
					name: (n.fname + (CASE WHEN n.mname IS NOT NULL THEN (" " + n.mname) ELSE "" END) + (CASE WHEN n.lname IS NOT NULL THEN (" " + n.lname) ELSE "" END)),
					movies: movies
				}) AS Actor
			'''

			results = self.session.run(actors_query, parameters={"idactors": idactors, "fname": fname, "lname": lname})

			results_list = []
			for record in results:
				results_list.append(record['Actor'])

			return results_list
		except Exception as err: return str(err)

	# GET SC 4
	def sc4(self, genre, start, end):
		genre = '(?i)' + genre
		try:
			# Genre exploration (SC4) query
			genre_query = '''
				MATCH 
					(n:genres)
				WHERE
					n.genre =~ {genre}
				WITH n
				OPTIONAL MATCH
					(n)-[:GENRES]-(m:movies_genres)
				OPTIONAL MATCH
					(m)-[:MOVIES]-(o:movies)
				WHERE
					CASE WHEN {end} IS NULL THEN
						o.year >= {start}
					ELSE o.year >= {start} AND o.year <= {end}
					END
				WITH o
				MATCH
					(o)-[:MOVIES]-(p:acted_in)
				WITH o, p
				ORDER BY o.year, o.title
				MATCH
					(p)-[:ACTORS]-(q:actors)
				WITH DISTINCT(q) as q, collect(DISTINCT { title: o.title, year: o.year }) as movies
				RETURN
				({
					fname: q.fname, lname: q.lname, gender: q.gender, movies: movies
				}) AS Actor
			'''

			results = self.session.run(genre_query, parameters={"genre": genre, "start": start, "end": end})

			results_list = []
			for record in results:
				results_list.append(record['Actor'])

			return results_list
		except Exception as err: return str(err)

	# GET SC 5
	def sc5(self, start, end):
		try:
			# Genre statistics (SC5) query
			genre_query = '''
				MATCH 
					(n:genres)
				WITH n
				OPTIONAL MATCH
					(n)-[:GENRES]-(m:movies_genres)
				OPTIONAL MATCH
					(m)-[:MOVIES]-(o:movies)
				WHERE
					CASE WHEN {end} IS NULL THEN
						o.year >= {start}
					ELSE o.year >= {start} AND o.year <= {end}
					END
				WITH n, count(DISTINCT o) as movies
				RETURN
				({
					genre: n.genre, movies: movies
				}) AS Genre
			'''

			results = self.session.run(genre_query, parameters={"start": start, "end": end})

			results_list = []
			for record in results:
				results_list.append(record['Genre'])

			return results_list
		except Exception as err: return str(err)